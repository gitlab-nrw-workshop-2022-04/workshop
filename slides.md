title: Einführung in git und FDM mit GitLab
layout: true
name: nfdi4ing-template
class: nfdi4ing
<div class="slide-layout">
    <div class="slide-header"></div>
    <div class="slide-footer">29.04.2022 — Seite</div>
    <a class="slide-license" href="https://creativecommons.org/licenses/by-sa/4.0" target="_blank" title="CC-BY-SA 4.0"></a>
</div>
---
class: title-red

# Einführung in git und FDM mit GitLab

Marius Politze, RWTH Aachen University

Henning Timm, Universität Duisburg-Essen

29.04.2021 12:00 - 17:00

<span style="float:right;"> ![](img/partner.drawio.png) </span>

---
class: dense

# Agenda

* 12:00 - 13:00 - Block 1
  * 12:00 - Welcome
  * 12:10 - Recap git
  * 12:30 - Shaun das Schaf
* 13:00 - 13:20 - *PAUSE*
* 13:20 - 15:00 - Block 2 
  * 13:20 - Walkthrough GitLab
  * 13:40 - Git Clone mit https und PATs
  * 14:00 - Git GUIs: Tortoise Git, VSCode
  * 14:30 - GitTacToe
* 15:00 - 15:30 - *PAUSE*
* 15:30 - 17:00 - Block 3
  * 15:30 - FAIR Principles mit git
  * 15:50 - GitLab RDM Flow: Validate, Collaborate, Publish
  * 16:50 - Wrap Up & Goodbye

---
class: sparse

# Ergebnissicherung der Online-Vorbereitung

* Flipped Classroom Inhalte in GitLab

> https://gitlab-nrw-workshop-2022-04.gitlab.io/preparation/

* Rückfragen zu den Inhalten?
* Ideenspeicher für Fragen

> https://miro.com/app/board/uXjVO-jTN08=/?moveToWidget=3458764522736056857&cot=10

* Fehler bei der Installation? Abhilfe für Heute

> https://repl.it/@mpolitze/GitLab-NRW-Workshop

---


# Git: Remote Version Control

.center[
  ![Remote Version Control](img/remote-version-control.svg)
]


---


# Demo: Git Ablauf (2)

.center[
  ![Git Workflow](img/git-workflow.svg)
]

---
class: wide

.center[
  [![Explain git with D3](img/explaingitwithd3.png)](http://onlywei.github.io/explain-git-with-d3/)
]


---

# Git Grundlegende Befehle - *Globale Konfiguration*

## Konfiguration 1x pro Computer erforderlich:

Konfiguriert `<Benutername>` als globalen Git-Benutzernamen
```bash
git config --global user.name <Benutzername>
# z.B.
git config --global user.name "Marius Politze"
```

Konfiguriert `<Email-Adresse>` als globale Git-Emailadresse
```bash
git config --global user.email <Email-Adresse>
# z.B.
git config --global user.email "politze@itc.rwth-aachen.de"
```

---
class: small, wide, cols-2

# Git Grundlegende Befehle - *Interaktion mit dem Repository*

Initialisiert im aktuellen Verzeichnis ein git Repository
```bash
git init
# z.B.
git init
```

Fügt Dateien dem staging Bereich hinzu
```bash
git add
# z.B.
git add .
```

Commitet die Dateien aus dem staging Bereich ins Repository
```bash
git commit -m <commit message>
#z.B.
git commit –m "Erster Commit"
```

<div class="col-break"></div>

Klont ein entferntes Repository
```bash
git clone <repository url>
# z.B.
git clone https://git.rwth-aachen.de/grp/repo.git
```

Versionshistorie an das entfernte Repository senden
```bash
git push <repository url> <branch>
# z.B.
git push https://git.rwth-aachen.de/grp/repo.git master
# oder den aktuellen branch zum default remote
git push
```

Holt den Versionsstand aus dem entfernten Repository und mergt in das Arbeitsverzeichnis
```bash
git pull <repository url> <branch>
# z.B.
git pull https://git.rwth-aachen.de/grp/repo.git master
# oder den aktuellen branch vom default remote
git pull
```

---
class: wide

<p style="margin-top:-3em">
.center[
![Git Cheat Sheet by by Hylke Bons based on work by Zack Rusin and Sébastien Pierre. This work is licensed under the Creative Commons Attribution 3.0 License.](https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png)
]

---
class: dense

# Shaun das Schaf

* Concept Board
  * https://app.conceptboard.com/board/6fsh-e47b-p4pa-nh6k-tnbe#
* Further reading
  * Git Documentation: https://git-scm.com/docs
  * Interactive tutorials for advanced concept, e.g. branching, merging: https://learngitbranching.js.org/
  * Git Purr: Git Commands Explained with Cats: https://girliemac.com/blog/2017/12/26/git-purr/
  * Git-Schnitzeljagd: https://git.rwth-aachen.de/patrick.jueptner/git-hunt-lvl1-7

---
class: img-only

![Results mapping git repository to Shaun the Sheep](img/2022-04-29-12-55-20.png)


---
class: title-blue, img-only

# *Pause*

![](img/pause1.jpg)

Foto: Marco Verch | [ccnull.de](https://ccnull.de/foto/coffe-break-concept-with-cup-of-coffee-and-mobile-phone-on-the-table/1013743) | CC-BY 2.0

---
class: title-green

# GitLab Walkthrough

---
class: dense 

# GitLab Walkthrough: *Wiki*

## Zweck: Dokumentation der Daten im Repository
* Sind selbst ein git Repository
* Seiten können verlinkt werden
* Markdown ermöglicht Formatierung
* Bilder, Videos und Quellcode können eingefügt werden

--

## Demo: Wikis in GitLab
* Erstellen von Seiten
* Bearbeiten von Seiten
* Markdown:
  * Überschriften
  * Formatierung
  * Links
  * Listen
  * Quellcode
  * Bilder und Videos

---
class: dense

# GitLab Walkthrough: *Issues*

## Zweck: Projektmanagement direkt im Repository
* Einzelne Arbeitspakete
* Lassen sich Projektmitgliedern zuweisen
* Lassen sich zu Meilensteinen zusammenfassen
* Verschiedene Ansichten

--

## Demo: Issues in GitLab
* Erstellen von Issues
  * Beschreibung
  * Labels
  * Metadaten
* Arbeitspakete Planen
  * Mit dem Meilensteinen
  * Mit Boards
Mit dem Repository verknüpfen
  * Mit Commits
  * Mit Branches
  * Mit Merge Requests

---
class: dense

# GitLab Walkthrough: *CI/CD*

## Zweck: Ausführen von Workflows mit den Dateien im Repository
* Definition des Workflows im Repository
* Ausführung automatisch bei Änderungen im Repository
* Regelmäßige Ausführung ("cron")
* Automatisierung von Abläufen

--

## Demo: CI/CI in GitLab
* Erstellen einer Workflow-Definition
  * Datei `.gitlab-ci.yml`
* Benötigt einen *GitLab Runner*
  * In der Cloud
  * Auf einem lokalen Server
  * Auf dem eigenen Computer
* Ergebnisse einsehen
  * Im Repository
  * Job Details

---
class: sparse

# `git clone` mit *https* und *PAT*s

* Zwei verschiedene methoden zum Austauschen von Versionen
  * SSH - Zugriff über privaten kryptographischen Schlüssel
  * HTTPS - Zugriff über Nutzername und Passwort
* *Git Credential Manager Core* speichert HTTPS-Passwort lokal
  * Installation unter Windows standard
  * Installation prüfen: `git credential-manager-core --version`
* *PAT*: Personal Access Token
  * Alternative zum Passwort zum Speichern auf dem Computer
  * Wird (einzeln) im Nutzerprofil erstellt
  * Notwendige Rechte `read_repository`, `write_repository`
  * Kann jederzeit (einzeln) deaktiviert werden
  * Erhöht Sicherheit ggü. Nutzung von Passworten

---
class: title-green

# Git GUIs



---
class: sparse, img-right

# Git GUIs: *TortoiseGit*

![](img/tortoisegit-menu.png)

* Open Source Git GUI für Windows
  * https://tortoisegit.org/
* Integration in den Windows Explorer
  * `git status` über markierungen an Icons
  * Einzelne `git` Aktionen im Kontextmenü
  * Kenntnis der `git`-Befehle hilft

---
class: sparse, img-right

# Git GUIs: *VSCode* / *VSCodium*

![](img/vscode.drawio.png)

* Open Source Programmierumgebung
  * Plattformübergreifend (Windows, Mac, Linux)
  * https://code.visualstudio.com/ bzw. https://vscodium.com/
* Erweiterbar über Plugins
  * [GitGraph](https://open-vsx.org/extension/mhutchie/git-graph)
  * [GitDoc](https://open-vsx.org/extension/vsls-contrib/gitdoc)
  * uvm.
* Verbindet (Text)Editor und git GUI
* Kann auch zusammen mit anderen Editoren verwendet werden

---
class: title-green

# Übung: Git Tac Toe

---
class: img-right

# Übung: Git Tac Toe - Regeln

![Tic Tac Toe](img/tictactoe.svg)

* 2 Spieler: `X` und `O`
* Auf einem Feld 3x3
* Spieler ziehen gleichzeitig(!)
* In jedem Zug setzt der Spieler sein Symbol in ein Feld
* Gewonnen hat, wer drei Symbole in einer Zeile, Spalte oder Diagonale hat

In einer Textdatei sieht das Spielfeld dann so aus:

```python
 | | 
-+-+-
 | | 
-+-+-
 | | 
```

---
class: dense

# Übung: Git Tac Toe - *Gleichzeitig (1)*

Vorbereitung: `X` erstellt eine neue Datei `spielfeld.txt`, füllt diese mit dem Spielfeld

```bash
X> git add Runde2.txt
X> git commit -m "Spielfeld für GitTacToe"
X> git push
```

Spielzug gleichzeitig: `X` und `O` machen ihre Züge

```bash
X&O> git pull
```

> Zug im Spielfeld eintragen

```bash
X&0> git add spielfeld.txt
X&O> git commit –m "{X,O} Zug 1"
```

---
class: dense

# Übung: Git Tac Toe - *Gleichzeitig (2)*

Spielzug nacheinander: Änderungen an den Server senden (Erst Spieler `O`!)

```bash
X&O> git push
```

> *Der push von X wird rejected!*

`X` muss den Konflikt lösen:

```bash
X> git pull
```

> Entweder: git macht einen automatischen merge
> Oder: `X` öffnet `spielfeld.txt` und führt die Spielstände zusammen.

```bash
X> git add spielfeld.txt
X> git commit -m "Zug 1 zusammengeführt"
X> git push
```
... und so weiter ... 

---
class: title-blue, img-only

# *Pause*

![](img/pause2.jpg)

Foto: Marco Verch | [ccnull.de](https://ccnull.de/foto/klebezettel-mit-dem-wort-pause-an-einer-weckuhr/1008985) | CC-BY 2.0

---
class: title-green

# FAIR Principles mit git


---
class: dense

# Recap FAIR Principles *(1)*
## To be Findable:

* F1. (Meta)data are assigned a globally unique and persistent identifier
* F2. Data are described with rich metadata
* F3. Metadata clearly and explicitly include the identifier of the data they describe
* F4. (Meta)data are registered or indexed in a searchable resource

--

## To be Accessible:

* A1. (Meta)data are retrievable by their identifier using a standardised communications protocol
  * A1.1 The protocol is open, free, and universally implementable
  * A1.2 The protocol allows for an authentication and authorisation procedure, where necessary
* A2. Metadata are accessible, even when the data are no longer available

---
class: dense

# Recap FAIR Principles *(2)*

## To be Interoperable:

* I1. (Meta)data use a formal, accessible, shared, and broadly applicable language for knowledge representation.
* I2. (Meta)data use vocabularies that follow FAIR principles
* I3. (Meta)data include qualified references to other (meta)data

--

## To be Re-usable:

* R1. (Meta)data are richly described with a plurality of accurate and relevant attributes
  * R1.1. (Meta)data are released with a clear and accessible data usage license
  * R1.2. (Meta)data are associated with detailed provenance
  * R1.3. (Meta)data meet domain-relevant community standards

---
class: dense

# Wo finden sich die FAIR Principles in git und GitLab? *(1)*

## Findable

* Jedes Projekt erhält eine eindeutige URL (F1)
* Im `git log` können Dateien in allen Versionen über den Versionshash aufgefunden werden (F3)
* Über den Versionshash können alle Dateiinhalte abgerufen werden (F1)
* GitLab Indiziert Projekte und macht diese in einer Suchmaschine verfügbar (F4)

--

## Accessible

* Daten können über SSH und HTTPS abgerufen werden (A1.1)
* Beide Protokolle unterstützen Authentifizierungsmechanismen (A1.2)

---
class: dense

# Wo finden sich die FAIR Principles in git und GitLab? *(2)*

## Interoperable
* Erkennung von Speziellen Dateien (z.B. `Readme.md`, `LICENSE`) und Umwandlung in strukturierte Metadaten (I3)
* Nutzen offene, textbasierte Dateiformate (Text, Markdown, YAML) (I1)
* Erkennung bestimmter Metadatenformate z.B. YAML-Frontmatter (I2)

--

## Reusable

* Versionierung ermöglicht Nachverfolgbarkeit (Provenienz) der Daten (R1.2)
* Lizenzinformationen fest verankert in der UI von GitLab (R1.1)

---
class: sparse

# FAIR Principles in git und GitLab - Was fehlt? 

--

* Disziplinspezifische "rich" Metadaten (F2, I2, R1.3)
  * GitLab Metadaten sind bezogen auf Softwareentwicklung
  * Verfügbare Metadatenfelder sind daher recht generisch: Lizenzen, Autoren, Versionen
* Nachweis im wissenschaftlichen Sinne (F1, A2)
  * URLs in GitLab sind nicht für die Ewigkeit ("eternally persistent")
  * GitLab Projekte sind ohne weiteres nicht Zitierbar
* Achtung! git ist nicht für alle Datenformate gleich gut geeignet!

---
class: sparse

# git und GitLab FAIRbessern - RDM Flow: *Validate, Collaborate, Publish*

* Collaborative live demo
* Interessante Repositories
  * https://gitlab.com/fdm-nrw-gitlabexamples/index
  * https://gitlab.com/gitlab-nrw-workshop-2022-04/rdmflow

---
class: sparse, img-right

# Wrap Up

![Feedback Starfish](img/starfish.svg)

* Ideenspeicher

> https://miro.com/app/board/o9J_l1ta3sw=/?moveToWidget=3074457362682111842

* Fragen?
* Feedback Seestern

> https://miro.com/app/board/o9J_l1ta3sw=/?moveToWidget=3074457362682111898
